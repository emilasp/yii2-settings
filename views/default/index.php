<?php
use emilasp\settings\extensions\settings\SettingsWidget;

/* @var $this yii\web\View */

$this->title                   = Yii::t('settings', 'Settings');
$this->params['breadcrumbs'][] = $this->title;

echo SettingsWidget::widget();
