Модуль Settings для Yii2
=============================

Модуль для работы с различными настройками. Функциональная часть и админка.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-settings": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-settings.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'settings' => []
    ],
```
