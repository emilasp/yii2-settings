<?php
namespace emilasp\settings\extensions\settings;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class SettingsWidgetAsset
 * @package emilasp\extensions\settings
 */
class SettingsWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js = [
        'settings.js'
    ];
    //public $css = ['list.css'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    //public $jsOptions = ['position' => View::POS_END];
}
