$(".setting-save").on("click", function() {

    var button = $(this);
    var data = button.data();
    //var row = button.parent().parent();
    var value = button.parent().parent().find('input, select').val();

    console.log(value);

    button.removeClass('btn-primary');
    button.removeClass('btn-danger');
    button.removeClass('btn-success');
    button.addClass('btn-warning');

    var id = data['id'];
    var type = data['type'];
    var object = data['object'];
    var name = data['name'];
    var code = data['code'];

    console.log('id='+id+'type='+type+'&object='+object+'&code='+code+'&name='+name+'&value='+value);

    $.ajax({
        type: 'POST',
        url: '/settings/default/save-setting.html',
        dataType: "json",
        data: 'id='+id+'&type='+type+'&object='+object+'&code='+code+'&value='+value,
        success: function(msg) {
           // console.log(msg);

            notice(msg.message, msg.status == 1 ? 'green' : 'red');

            button.removeClass('btn-danger');
            button.removeClass('btn-info');
            button.removeClass('btn-warning');
            button.addClass('btn-success');
        },
        error: function(){
            //console.log('error');
            notice('Не удалось сохранить настройку', 'red');
            button.removeClass('btn-success');
            button.removeClass('btn-info');
            button.removeClass('btn-warning');
            button.addClass('danger');
        }
    });
});
