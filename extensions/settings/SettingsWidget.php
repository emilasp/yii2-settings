<?php

namespace emilasp\settings\extensions\settings;

use emilasp\settings\models\Setting;
use frontend\modules\core\helpers\FormatterHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii;

/** Виджет выводит список полей настроек
 * Class SettingsWidget
 * @package emilasp\settings\extensions\settings
 */
class SettingsWidget extends \yii\base\Widget
{
    public $model;

    public function init()
    {
        parent::init();
        $this->registerAssets();
    }

    /**
     * RUN
     */
    public function run()
    {
        if (!$this->model) {
            echo $this->render('objects', ['objects' => $this->getGlobalSettings()]);
        } else {
            echo $this->render('_object', [
                'object'     => $this->model,
                'categories' => $this->getSettings()
            ]);
        }
    }

    /** Получаем массив с именами классов из конфигурации модуля и получаем по ним настройки
     * @return array
     */
    private function getGlobalSettings()
    {
        $objects  = Yii::$app->getModule('settings')->onAdminPage;
        $settings = [];
        foreach ($objects as $class) {
            $settings[] = new $class(Inflector::classify($class));
        }
        return $settings;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        SettingsWidgetAsset::register($this->getView());
    }

    /**
     * Возвращаем настройки для модели сгруппированные по категориям
     *
     * @return array
     */
    private function getSettings(): array
    {
        $settings = $this->model->getSetting(null, true, false);
        return ArrayHelper::index($settings, null, 'category');
    }
}
