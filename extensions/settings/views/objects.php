<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;

?>
<div class="settings-index">

    <div class="row">
        <div class="col-md-3">

            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <?php foreach ($objects as $index => $object) : ?>
                    <a class="nav-link <?= !$index ? 'active' : '' ?>"
                       id="<?= Inflector::classify($object::className()) ?>-tab" data-toggle="pill"
                       href="#<?= Inflector::classify($object::className()) ?>" role="tab"
                       aria-controls="<?= Inflector::classify($object::className()) ?>"
                       aria-selected="<?= !$index ? 'true' : 'false' ?>">
                        <?= $object->meta['name'] ?>
                    </a>
                <?php endforeach ?>
            </div>

        </div>
        <div class="col-md-9">

            <div class="tab-content" id="v-pills-tabContent">
                <?php foreach ($objects as $index => $object) : ?>
                    <div class="tab-pane fade show <?= !$index ? 'show active' : '' ?>"
                         id="<?= Inflector::classify($object::className()) ?>" role="tabpanel"
                         aria-labelledby="<?= Inflector::classify($object::className()) ?>-tab">
                        <?= $this->render('_object', [
                            'object'     => $object,
                            'categories' => ArrayHelper::index($object->getSetting(null, true, false), null, 'category')
                        ]) ?>
                    </div>
                <?php endforeach ?>

            </div>
        </div>
    </div>





</div>
