<?php

use emilasp\settings\behaviors\SettingsBehavior;
use emilasp\settings\extensions\settings\SettingsWidget;
use kartik\date\DatePicker;
use yii\helpers\Html;

?>
<?php foreach ($categories as $category => $settings) : ?>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('settings', $category) ?></h3>
        </div>
        <div class="panel-body">
            <?php foreach ($settings as $index => $setting) : ?>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group field-settingsform-formparams-redirect_after_login">
                            <label class="control-label" for="settingsform-formparams-<?= $setting['code'] ?>">
                                <b><?= $setting['name'] ?></b>
                            </label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-gear"></i>
                                    </div>
                                </div>

                                <?php
                                switch ($setting['type']) {
                                    case SettingsBehavior::TYPE_TEXT:
                                        echo Html::textInput(
                                            'setting_' . $index,
                                            $setting['value'],
                                            ['class' => 'form-control']
                                        );
                                        break;
                                    case SettingsBehavior::TYPE_SELECT:
                                        echo Html::dropDownList(
                                            'setting_' . $index,
                                            $setting['value'],
                                            $setting['data'],
                                            ['class' => 'form-control']
                                        );
                                        break;
                                    case SettingsBehavior::TYPE_DATE:
                                        echo DatePicker::widget([
                                            'name'          => 'setting_' . $index,
                                            'value'         => $setting['value'],
                                            'options'       => ['placeholder' => 'Выберите дату..'],
                                            'language'      => 'ru-RU',
                                            'pluginOptions' => [
                                                'format'         => 'yyyy-mm-dd',
                                                'todayHighlight' => true
                                            ]
                                        ]);
                                        break;
                                }
                                ?>
                                <span class="input-group-btn">
                                <button class="btn btn-primary setting-save"
                                        data-object="<?= $object::className() ?>"
                                        data-id="<?= (!empty($object)) ? $object->id : '' ?>"
                                        data-code="<?= $setting['code'] ?>"
                                        data-name="<?= $setting['name'] ?>"
                                        data-type="<?= $object->meta['type'] ?>"
                                        type="button">
                                    Сохранить
                                </button>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label class="control-label" for="settingsform-formparams-<?= $setting['code'] ?>"></label>
                        <p class="help-block text-muted"><?= $setting['description'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>

<?php endforeach ?>
