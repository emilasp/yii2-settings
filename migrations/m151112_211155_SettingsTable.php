<?php

use yii\db\Migration;

/** ./yii migrate --migrationPath=./vendor/emilasp/yii2-settings/migration
 * Class m151112_211155_SettingsTable
 */
class m151112_211155_SettingsTable extends Migration
{
    private $tableOptions = null;

    public function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
    }

    public function up()
    {
        $this->createTable('settings_setting', [
            'id'         => $this->primaryKey(11),
            'type'       => $this->smallInteger(1)->notNull(),
            'object'     => $this->string(128)->notNull(),
            'object_id'  => $this->integer(11),
            'category'   => $this->string(20),
            'code'       => $this->string(50)->notNull(),
            'value'      => $this->string(255),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->createIndex('settings_code', 'settings_setting', 'code');
        $this->createIndex('settings_created_at', 'settings_setting', 'created_at');
        $this->createIndex('settings_object', 'settings_setting', ['type', 'object', 'object_id']);
    }

    public function down()
    {
        $this->dropTable('settings_setting');
        return true;
    }
}
