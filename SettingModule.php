<?php
namespace emilasp\settings;

use emilasp\core\CoreModule;

/**
 * Class SettingModule
 * @package emilasp\settings
 */
class SettingModule extends CoreModule
{
    /** @var array массив классов дял которых строим административную страницу */
    public $onAdminPage = [];
}
