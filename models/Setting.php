<?php

namespace emilasp\settings\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "settings_setting".
 *
 * @property integer $id
 * @property integer $type
 * @property string  $object
 * @property string  $object_id
 * @property string  $category
 * @property string  $code
 * @property string  $value
 * @property string  $created_at
 * @property string  $updated_at
 */
class Setting extends \emilasp\core\components\base\ActiveRecord
{
    /** Тип объекта */
    const TYPE_MODEL  = 1;
    const TYPE_MODULE = 2;
    const TYPE_OTHER  = 3;

    /** Значения по умолчанию для списка выбора из Да и Нет */
    const DEFAULT_SELECT_YES = 1;
    const DEFAULT_SELECT_NO  = 0;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings_setting';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'object', 'code'], 'required'],
            [['type', 'object_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['object'], 'string', 'max' => 128],
            [['code', 'category'], 'string', 'max' => 50],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'type'       => Yii::t('site', 'Type'),
            'object'     => Yii::t('site', 'Object'),
            'object_id'  => Yii::t('site', 'Object ID'),
            'code'       => Yii::t('site', 'Code'),
            'category'   => Yii::t('site', 'Category'),
            'value'      => Yii::t('site', 'Value'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
        ];
    }
}
