<?php
namespace emilasp\settings\controllers;

use emilasp\settings\models\Setting;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use emilasp\core\components\base\Controller;
use yii\helpers\Inflector;
use yii\helpers\Json;

/**
 * DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['index', 'save-setting'],
                'rules' => [
                    [
                        'actions' => ['index', 'save-setting'],
                        'allow'   => true,
                        'roles'   => ['admin'],
                    ],
                ],
                //'denyCallback' => Yii::$app->getModule('user')->denyCallback
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'save-setting' => ['post'],
                ],
            ],
        ];
    }

    /** Выводим страницу с табами настроек
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /** Сохраняем настройку
     * @return mixed
     */
    public function actionSaveSetting()
    {
        $objectId    = Yii::$app->request->post('id', null);
        $objectClass = Yii::$app->request->post('object');
        $code        = Yii::$app->request->post('code');
        $value       = Yii::$app->request->post('value');
        $type        = Yii::$app->request->post('type');

        if ($type === (string)Setting::TYPE_MODULE) {
            $object = new $objectClass(Inflector::classify($objectClass));
        } else {
            $object = $objectClass::findOne($objectId);
        }

        $isSave = $object->setSetting($code, $value, $objectId);

        if (!$isSave) {
            return $this->setAjaxResponse(self::AJAX_STATUS_ERROR, Yii::t('site', 'Update Error'));
        }

        return $this->setAjaxResponse(self::AJAX_STATUS_SUCCESS, Yii::t('site', 'Update Success'));
    }
}
