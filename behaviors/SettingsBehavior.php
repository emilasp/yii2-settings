<?php

namespace emilasp\settings\behaviors;

use yii;
use yii\base\Behavior;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use emilasp\settings\models\Setting;

/**
 * Добавляем в настройки модуля класс объекта в свойство onAdminPage(опционально)
 *
 * 'settings'   => [
 *     'class'       => 'emilasp\settings\SettingModule',
 *     'onAdminPage' => [
 *         User::className(),
 *         SiteModule::className(),
 *     ],
 * ],
 *
 * Затем добавляем поведение к объекту:
 *
 * function behaviors()
 * {
 *     return ArrayHelper::merge([
 *         'setting' => [
 *             'class'    => SettingsBehavior::className(),
 *             'meta'     => [
 *                 'name' => 'Пользователи',
 *                 'type' => Setting::TYPE_MODEL,
 *             ],
 *             'settings' => [
 *                 [
 *                     'code'    => 'user_hellow',
 *                     'name'    => 'Приветствие',
 *                     'default' => 'Здравствуй уважаемый пользователь',
 *                 ],
 *                 [
 *                     'code'        => 'allow_client_sale',
 *                     'name'        => 'Разрешить онлайн продажи.',
 *                     'description' => 'Разрешить онлайн продажи.',
 *                     'default'     => '',
 *                     'data'      => [
 *                         ''                          => 'По умолчанию',
 *                         Setting::DEFAULT_SELECT_NO  => 'Нет',
 *                         Setting::DEFAULT_SELECT_YES => 'Да',
 *                         3                           => 'Не знаю',
 *                     ],
 *                 ],
 *             ],
 *         ],
 *     ], parent::behaviors());
 * }
 *
 * Доступ к настройкам по ссылке: settings/default/index.html
 *
 * Class SettingsBehavior
 * @package emilasp\settings\behaviors
 */
class SettingsBehavior extends Behavior
{
    public const TYPE_TEXT     = 'text';
    public const TYPE_SELECT   = 'select';
    public const TYPE_DATE     = 'date';
    public const TYPE_DATETIME = 'datetime';

    /** @var  array массив с глобальными данными - тип(модель,модуль), наименование для админки */
    public $meta;
    /** @var  array настройки */
    public $settings;
    /** @var string ID атрибут для объекта с типом модель */
    public $objectIdAttr = 'id';

    private $defaultMeta = [
        'type' => Setting::TYPE_OTHER,
    ];

    /** @var int время сохранения настроек в кеше */
    public $cacheDuration = 15;

    /** Формируем мета для объекта */
    private function setMeta()
    {
        $object                    = $this->owner;
        $this->defaultMeta['name'] = $object::className();
        $this->meta                = ArrayHelper::merge($this->defaultMeta, $this->meta);
    }

    /** Получаем настройку по коду или если код не передан - все настройки для объекта
     *
     * @param null      $code
     * @param bool|true $asValue
     * @param bool|true $cache
     *
     * @return array
     */
    public function getSetting(string $code = null, bool $asValue = true, bool $cache = true, callable $filter = null)
    {
        $this->setMeta();
        $object = $this->owner;

        $conditionObjectId = '';
        if ($this->meta['type'] === Setting::TYPE_MODEL) {
            $conditionObjectId = ' AND object_id=\'' . $object->{$this->objectIdAttr} . '\'';
        }
        /** Получаем настройки для объекта */

        $sql = <<<SQL
            SELECT code, "value" FROM settings_setting 
            WHERE type='{$this->meta['type']}' AND object='{$this->getObjectName()}'
            {$conditionObjectId} 
            ORDER BY category;
SQL;

        if ($cache) {
            $duration   = $this->cacheDuration;
            $dependency = new DbDependency([
                'sql'      => 'SELECT MAX(updated_at) FROM settings_setting;',
                'reusable' => true
            ]);

            $objectSet = Yii::$app->db->cache(function () use ($sql) {
                return Yii::$app->db->createCommand($sql)->queryAll();
            }, $duration, $dependency);
        } else {
            $objectSet = Yii::$app->db->createCommand($sql)->queryAll();
        }

        /** @var  $data - */
        $data = $this->setValues(ArrayHelper::map($objectSet, 'code', 'value'));

        $return = $data;
        /** Если передан код, то отдаём только значение настройки дял этого кода */
        if ($code !== null) {
            $data = ArrayHelper::index($data, 'code');
            if ($asValue) {
                $return = $data[$code]['value'];
            } else {
                $return = $data[$code];
            }
        }

        if (is_callable($filter)) {
            return $filter($return);
        }
        return $return;
    }

    /** формируем массив настроек и отдаём его
     *
     * @param $values
     *
     * @return array
     */
    private function setValues($values)
    {
        $data = [];
        foreach ($this->settings as $index => $setting) {
            $cod                         = $setting['code'];
            $data[$index]['code']        = $cod;
            $data[$index]['type']        = $setting['type'] ?? self::TYPE_TEXT;
            $data[$index]['name']        = $setting['name'];
            $data[$index]['category']    = $setting['category'] ?? 'Default';
            $data[$index]['description'] = $setting['description'] ?? '';
            $data[$index]['data']        = $setting['data'] ?? '';
            $data[$index]['default']     = $setting['default'] ?? '';
            $data[$index]['value']       = $values[$cod] ?? $data[$index]['default'];
        }
        return $data;
    }

    /** Сохраняем настройку в базу
     *
     * @param string           $code
     * @param string|int|false $value
     * @param null|int         $objectId
     *
     * @return bool|false|int
     * @throws \Exception
     */
    public function setSetting($code, $value, $objectId = null)
    {
        $this->setMeta();

        $isSave = false;

        $settings    = $this->owner->setting;
        $objectClass = $this->getObjectName();
        $condition   = ['code' => $code, 'object' => $objectClass];

        if ($this->meta['type'] === Setting::TYPE_MODEL && $objectId) {
            $condition['object_id'] = $objectId;
        }

        /** @var Setting $setting */
        $setting  = Setting::findOne($condition);
        $default  = (string)($settings[$code]['default'] ?? null);
        $category = $settings[$code]['category'] ?? null;

        if ($setting && ($value === false || ($default !== null && $value === $default))) {
            $setting->delete();
        }

        if ($value !== false && $value !== $default) {
            if ($setting === null) {
                $setting           = new Setting();
                $setting->category = $category;
                $setting->code     = $code;
                $setting->object   = $objectClass;
                $setting->type     = $this->meta['type'];

                if ($this->meta['type'] === Setting::TYPE_MODEL && $objectId) {
                    $setting->object_id = $objectId;
                }
            }
            $setting->value = $value;
            $isSave         = $setting->save();
        } elseif ($value === $default) {
            $isSave = 2;
        }
        return $isSave;
    }

    /** Получаем имя Объека для идентификации настройки
     * @return string
     */
    private function getObjectName()
    {
        return (new \ReflectionClass($this->owner))->getShortName();
    }
}
